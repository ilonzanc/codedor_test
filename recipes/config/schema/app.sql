DROP TABLE IF EXISTS recipes;
DROP TABLE IF EXISTS categories;

CREATE TABLE categories (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    created DATETIME,
    modified DATETIME
);

CREATE TABLE recipes (
    id INT AUTO_INCREMENT PRIMARY KEY,
    category_id INT,
    name VARCHAR(50) NOT NULL,
    ingredients TEXT,
    steps TEXT,
    preptime INT,
    visible BOOLEAN,
    created DATETIME,
    modified DATETIME,
    FOREIGN KEY category_key (category_id) REFERENCES categories(id)
);