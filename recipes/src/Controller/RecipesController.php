<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Recipes Controller
 *
 * @property \App\Model\Table\RecipesTable $Recipes
 *
 * @method \App\Model\Entity\Recipe[] paginate($object = null, array $settings = [])
 */
class RecipesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Categories']
        ];

        $recipes = $this->Recipes->find('all');

        $recipes = $this->paginate($recipes);
        
        $this->set([
            'recipes' => $recipes,
            '_serialize' => ['recipes']
        ]);
    }

    /**
     * View method
     *
     * @param string|null $id Recipe id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $recipe = $this->Recipes->get($id, [
            'contain' => ['Categories']
        ]);

        $this->set([
            'recipe' => $recipe,
            '_serialize' => ['recipe']
        ]);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

    public function add()
    {
        $recipe = $this->Recipes->newEntity();

        if ($this->request->is('post')) {
            $ingredients = array();
            foreach($this->request->getData() as $name => $value) {
                if (strpos($name, 'ingredient') !== false) {
                    if (strpos($name, 'ingredients') === false) {
                        $item = $this->request->data[$name];
                        array_push($ingredients, $item);
                    }                    
                }                
            }
            $ingredients = implode(',', $ingredients);
            $this->request->data['ingredients'] = $ingredients;

            $steps = array();
            foreach($this->request->getData() as $name => $value) {
                if (strpos($name, 'step') !== false) {
                    if (strpos($name, 'steps') === false) {
                        $item = $this->request->data[$name];
                        array_push($steps, $item);
                    }                    
                }                
            }
            $steps = implode(',', $steps);
            $this->request->data['steps'] = $steps;

            $recipe = $this->Recipes->patchEntity($recipe, $this->request->getData());
            if ($this->Recipes->save($recipe)) {
                $this->Flash->success(__('The recipe has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The recipe could not be saved. Please, try again.'));
            }
        }
        $categories = $this->Recipes->Categories->find('list', ['limit' => 200]);
        $this->set([
            'recipe' => $recipe,
            'categories' => $categories,
            '_serialize' => ['recipe', 'categories']
        ]);
    }

    /**
     * Edit method
     *
     * @param string|null $id Recipe id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $recipe = $this->Recipes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $recipe = $this->Recipes->patchEntity($recipe, $this->request->getData());
            if ($this->Recipes->save($recipe)) {
                $this->Flash->success(__('The recipe has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The recipe could not be saved. Please, try again.'));
            }
        }
        $categories = $this->Recipes->Categories->find('list', ['limit' => 200]);
        $this->set([
            'recipe' => $recipe,
            'categories' => $categories,
            '_serialize' => ['recipe', 'categories']
        ]);
    }

    /**
     * Delete method
     *
     * @param string|null $id Recipe id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $recipe = $this->Recipes->get($id);
        if ($this->Recipes->delete($recipe)) {
            $this->Flash->success(__('The recipe has been deleted.'));
        } else {
            $this->Flash->error(__('The recipe could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
