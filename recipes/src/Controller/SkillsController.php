<?php
    namespace App\Controller;

    use App\Controller\AppController;

    class SkillsController extends AppController
    {
        public function index()
        {
            $this->set([
                'skills' => ['Laravel', 'Javascript', 'PHP'],
                '_serialize' => ['skills']
            ]);
        }        

    }
?>