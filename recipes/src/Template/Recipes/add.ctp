<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Recipe $recipe
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Recipes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="recipes form large-9 medium-8 columns content">
    <?= $this->Form->create($recipe) ?>
    <fieldset>
        <legend><?= __('Add Recipe') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('category_id', ['options' => $categories, 'empty' => false]);
        ?>
        <div class="container__ingredients">
            <label for="ingredient1">Ingredients</label> 
            <?php
                echo $this->Form->text('ingredient1');
                echo $this->Form->text('ingredient2');
            ?>
        </div>        
        <button class="addIngredientInput">add ingredient</button>
        <?php
            echo $this->Form->hidden('ingredients');
        ?>
        <div class="container__steps">
            <label for="step1">Steps</label> 
            <?php
                echo $this->Form->text('step1');
                echo $this->Form->text('step2');
            ?>
        </div>
        <button class="addStepInput">add step</button>
        <?php
            echo $this->Form->hidden('steps');
            echo $this->Form->control('preptime');
            echo $this->Form->control('visible');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
