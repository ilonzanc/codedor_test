(function () {
    console.log('Script file loaded');
    let buttonIngredient = document.querySelector('.addIngredientInput');
    let buttonStep = document.querySelector('.addStepInput');
    
    let ingredients = document.querySelectorAll('[name*="ingredient"');
    let steps = document.querySelectorAll('[name*="step"');

    let lastingredient = ingredients.length;
    let laststep = steps.length;
    if (buttonIngredient || buttonStep) {
        buttonIngredient.addEventListener('click', function (e) {
            e.preventDefault();            
            let container = document.querySelector('.container__ingredients');
            let newInput = document.createElement("input");
            newInput.type = "text";
            newInput.name = "ingredient" + lastingredient;
            lastingredient++;
            container.appendChild(newInput);
        });

        buttonStep.addEventListener('click', function (e) {
            e.preventDefault();            
            let container = document.querySelector('.container__steps');
            let newInput = document.createElement("input");
            newInput.type = "text";
            newInput.name = "step" + laststep;
            laststep++;
            container.appendChild(newInput);
        });
    }
})();