import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Recipe from '@/components/Recipe'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/recepten/:id',
      name: 'Recipe',
      component: Recipe
    }
  ]
})
